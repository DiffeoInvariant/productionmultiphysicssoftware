# HW 6
For homework 6, I used my PETSc-based copy of the ```fd``` code, contained in ```../petsc_fd```. The relevant code is contained in ```../petsc_fd/include/Utils.h```, ```../petsc_fd/tests/test_convergence.cc```, and ```../petsc_fd/makefile``` (specifically, the rule ```tests/test_convergence```). To make and view the convergence test report, first
```
cd ../pytex
sudo python3 setup.py install
```
to install pytex, which is necessary to write the test report (or, if you just want to view the report without making it on your machine, it's located at ```../petsc_fd/convergence_test_report.pdf```). Then, if you don't have PETSc and a recent clang (one that supports -std=c++20, which any recent version does) installed, you'll need those. Once that's all installed,
```
cd ../petsc_fd
make PETSC_DIR=/your/petsc-dir PETSC_ARCH=your-petsc-arch
open convergence_test_report.pdf
```