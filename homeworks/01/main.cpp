#include <iostream>
#include <vector>
#include <string>


using Mat = std::vector<std::vector<double>>;

void allocate_mat(Mat *mat, int nrow, int ncol) 
{
  /*note: this should be in a try-catch block to catch an std::bad_alloc*/
  mat->resize(nrow);
  for(auto row = mat->begin(); row != mat->end(); ++row) {
    row->resize(ncol);
  }
}

void fill_mat_values(Mat *mat)
{
  auto nrow = mat->size();
  auto ncol = (*mat)[0].size();
  for(auto i = 0; i < nrow; ++i) {
    for(auto j = 0; j < ncol; ++j) {
      (*mat)[i][j] = static_cast<double>((1 - i)*(1 + i) * (1 - j) * (1 + j));
    }
  }
}

void print_mat(const Mat& mat)
{
  for(auto i = 0; i < mat.size(); ++i) {
    for(auto j = 0; j < mat[0].size(); ++j) {
      std::cout << i << " " << j << " " << mat[i][j] << '\n';
    }
    std::cout << '\n';
  }
}


int main(int argc, char **argv)
{

  int nx = 5, ny = 5;
  Mat zValues;
  
  allocate_mat(&zValues,nx,ny);
  fill_mat_values(&zValues);
  print_mat(zValues);

  return 0;
}
  
      


  
