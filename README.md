# Production Multiphysics Software Development

## Building
```
make
```

## Running Homework
To run homework number 1, do
```
make
./homeworks/01/bin/main
```

Code related to homework 6 is contained in petsc_fd; first though, you should install pytex, by doing
```
cd pytex
sudo python3 setup.py install
```
Once you've done that, just ```cd``` into petsc_fd and
```
make
```