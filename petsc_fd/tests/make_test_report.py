#!/usr/bin/python3
import subprocess
from pytex import TestReport,Section,TextLines,TextModifier,UsePackage
from math import log,log10

def get_rms_error():
    rms_cmds = ['tests/convergence_test_rms']
    err = subprocess.run(rms_cmds,capture_output=True).stdout
    return [float(e) for e in err.split(b'\n')[:-1]]


def get_l2_error():
    rms_cmds = ['tests/convergence_test_l2']
    err = subprocess.run(rms_cmds,capture_output=True).stdout
    return [float(e) for e in err.split(b'\n')[:-1]]


def get_max_pointwise_error():
    rms_cmds = ['tests/convergence_test_maxpointwise']
    err = subprocess.run(rms_cmds,capture_output=True).stdout
    return [float(e) for e in err.split(b'\n')[:-1]]

def get_n():
    rms_cmds = ['tests/convergence_test_n']
    err = subprocess.run(rms_cmds,capture_output=True).stdout
    return [int(e) for e in err.split(b'\n')[:-1]]


def make_convergence_report():
    report = TestReport('tests/convergence_test_report','Convergence Test Report')
    report.add_section('Laplacian Finite Difference Convergence Test',['This plot shows the solution on a 640-by-640 grid.'])
    report.add_image('tests/plot8.png',scale=0.75,other_options=['H'])
    report.add_required_packages([UsePackage('amsmath')])
    logl2err = [str(log10(x)) for x in get_rms_error()]
    
    l2err = [str(x).replace('e-','\\cdot 10^{-') for x in get_rms_error()]
    l2err = [x + '}' if '10^' in x else x for x in l2err]
    n = get_n()
    el2 = get_rms_error()
    convrate1 = (log10(el2[1]) - log10(el2[0]))/(log10(1.0/n[1]) - log10(1.0/n[0]))
    convrate2 = (log10(el2[-1]) - log10(el2[0]))/(log10(1.0/n[-1]) - log10(1.0/n[0]))
    convrate = 0.5*(convrate1 + convrate2)
    import matplotlib.pyplot as plt
    mplfig = 'tests/convplot.png'
    plt.figure()
    plt.plot([log10(1.0/x) for x in n],[log10(x) for x in get_rms_error()])
    plt.title('log_{10}(e_{RMS}) versus log_{10}(1/N)')
    plt.xlabel('log_{10}(1/N)')
    plt.ylabel('log_{10}(e_{RMS})')
    plt.savefig(mplfig)
    
    report.append(TextLines(['Number of nodes on one side of the grid (in other words, the square root of the total number of nodes) for each test:\\\\'] + ['\\\\N and RMS error with increasing N (finer grid): \\\\']+['$(N,e_{L_2}) = (' + str(x[0])+', ' + x[1] + ')$\\\\' for x in zip(n,l2err)] + ['\\\\ $\\log_{10}(1/N)$ and $\\log_{10}(RMS)$ error with increasing N (finer grid): \\\\']+['$(\\log_{10}(1/N),\\log_{10}(e_{RMS})) = (' + str(log10(1.0/x[0]))+', ' + x[1] + ')$\\\\' for x in zip(n,logl2err)] + ['\\\\Below, we have a plot of the second table:']))
    report.add_image(mplfig,other_options=['H'])
    report.append(TextLines([f'The convergence rate is {convrate}.']))
    report.compile()



make_convergence_report()

