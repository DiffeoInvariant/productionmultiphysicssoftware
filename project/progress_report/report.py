from pytex import *

def make_report():
    from pytex import TestReport, Section
    secs = ['pr_sec_1.tex','pr_sec_2.tex']
    doc = TestReport('progress_report',required_packages=[UsePackage('url')])
    doc.set_title('CVEN 5838 Project Progress Report','Zane Jakobs',True)
    doc.append(Section.from_file(secs[0],section_name='Changes To Deliverables'))
    doc.append(Section.from_file(secs[1],'Current Progress'))
    doc.add_python_function(make_report,"A Python function to generate this report")
    compile_output = doc.compile().stdout
    print(f"Report generated. Compilation stdout:\n{compile_output}")

secs = ['pr_sec_1.tex','pr_sec_2.tex']
doc = TestReport('progress_report',required_packages=[UsePackage('url')])
doc.set_title('CVEN 5838 Project Progress Report','Zane Jakobs',True)
doc.append(Section.from_file(secs[0],section_name='Changes To Deliverables'))
doc.append(Section.from_file(secs[1],'Current Progress'))
doc.add_python_function(make_report,"A Python function to generate this report")
compile_output = doc.compile().stdout
print(f"Report generated. Compilation stdout:\n{compile_output}")


